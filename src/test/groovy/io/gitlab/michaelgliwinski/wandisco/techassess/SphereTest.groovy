package io.gitlab.michaelgliwinski.wandisco.techassess

import spock.lang.Unroll

class SphereTest extends ShapeTestBase {
    @Unroll
    def "it asserts a sphere of radius #radius has volume #volume"(double radius, double volume) {
        given:
        def sphere = new Sphere(radius)

        expect:
        roundTo2(sphere.volume) == volume

        where:
        radius | volume
        1      | 4.19
        5      | 523.6
    }
}
