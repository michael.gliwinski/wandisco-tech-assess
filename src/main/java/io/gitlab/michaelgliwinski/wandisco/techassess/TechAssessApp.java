package io.gitlab.michaelgliwinski.wandisco.techassess;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TechAssessApp {
    public static void main(String[] args) {
        getShapes().forEach(shape ->
                System.out.format("shape %s has volume %.2f\n",
                        shape, shape.getVolume()));
    }

    private static Stream<? extends Shape> getShapes() {
        List<Integer> widths = Arrays.asList(1, 5);

        Stream<Sphere> spheres = widths.stream().map(Sphere::new);
        Stream<Cube> cubes = widths.stream().map(Cube::new);
        Stream<RegularTetrahedron> tetrahedrons = widths.stream().map(RegularTetrahedron::new);

        return Stream.of(spheres, cubes, tetrahedrons).flatMap(s -> s);
    }
}
