package io.gitlab.michaelgliwinski.wandisco.techassess;

import com.google.common.base.Objects;

public class Sphere implements Shape {
    private final double radius;

    public Sphere(double radius) {
        this.radius = radius;
    }

    private double getRadius() {
        return radius;
    }

    /**
     * Return the volume of the sphere
     * <p>
     * The formula is: \(4/3 \pi radius^3\)
     *
     * @return the volume
     */
    @Override
    public double getVolume() {
        return (4.0 / 3) * Math.PI * Math.pow(getRadius(), 3);
    }

    @Override
    public String toString() {
        return "Sphere(radius=" + getRadius() + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sphere sphere = (Sphere) o;
        return Double.compare(sphere.getRadius(), getRadius()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRadius());
    }
}
